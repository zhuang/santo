# santo
A demo for comparing the solution with and without using Worker

## Installation
1. run `npm install -g local-web-server`
2. go to the project dir, run `ws`
3. compare *127.0.0.1/index.html* (with Worker) and *127.0.0.1/index2.html* (without Worker)
